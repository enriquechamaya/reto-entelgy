package reto.entelgy.retoentelgy.response;

public class GenericoResponse {

	private Object data;

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
