package reto.entelgy.retoentelgy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import reto.entelgy.retoentelgy.dto.UsuarioDTO;
import reto.entelgy.retoentelgy.response.GenericoResponse;
import reto.entelgy.retoentelgy.service.UsuarioService;

import java.util.ArrayList;
import java.util.LinkedHashMap;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Value("${endpoint}")
	private String endpoint;

	@Autowired
	RestTemplate restTemplate;

	@Override
	public GenericoResponse listar() throws Exception {
		GenericoResponse response = new GenericoResponse();
		HttpHeaders headers = new HttpHeaders();
		headers.add("user-agent", "Application");
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Object> responseEntity = restTemplate.exchange(endpoint, HttpMethod.GET, entity, Object.class);
		Object responseBody = responseEntity.getBody();
		ArrayList<LinkedHashMap> lista = (ArrayList<LinkedHashMap>) ((LinkedHashMap) responseBody).get("data");
		response.setData(obtenerUsuariosDTO(lista));
		return response;
	}

	private ArrayList<UsuarioDTO> obtenerUsuariosDTO(ArrayList<LinkedHashMap> lista) {
		ArrayList<UsuarioDTO> usuarios = new ArrayList<>();
		for (LinkedHashMap u : lista) {
			UsuarioDTO usuario = new UsuarioDTO();
			usuario.setId((int) u.get("id"));
			usuario.setLast_name(u.get("last_name").toString());
			usuario.setEmail(u.get("email").toString());
			usuarios.add(usuario);
		}
		return usuarios;
	}

}
