package reto.entelgy.retoentelgy.service;

import reto.entelgy.retoentelgy.response.GenericoResponse;

public interface UsuarioService {

	GenericoResponse listar() throws Exception;

}
