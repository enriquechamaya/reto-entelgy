package reto.entelgy.retoentelgy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reto.entelgy.retoentelgy.response.GenericoResponse;
import reto.entelgy.retoentelgy.service.UsuarioService;

@RestController
@RequestMapping("/v1/usuarios")
@CrossOrigin(origins = "*")
public class UsuarioControlador {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping
	public ResponseEntity<GenericoResponse> listarUsuarios() throws Exception {
		GenericoResponse response = usuarioService.listar();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
