package reto.entelgy.retoentelgy.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import reto.entelgy.retoentelgy.response.GenericoResponse;
import reto.entelgy.retoentelgy.service.UsuarioService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UsuarioControlador.class)
class UsuarioControllerTest {

	@Autowired
	private UsuarioControlador usuarioControlador;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UsuarioService usuarioService;

	@Test
	void contextLoads() {
		assertThat(usuarioControlador).isNotNull();
	}

	@Test
	void listarTest() throws Exception {
		GenericoResponse response = new GenericoResponse();

		when(usuarioService.listar()).thenReturn(response);

		mockMvc.perform(MockMvcRequestBuilders.get("/v1/usuarios"))
				.andExpect(status().isOk());

	}


}
