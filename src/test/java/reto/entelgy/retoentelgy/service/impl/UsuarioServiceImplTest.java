package reto.entelgy.retoentelgy.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reto.entelgy.retoentelgy.dto.UsuarioDTO;
import reto.entelgy.retoentelgy.response.GenericoResponse;
import reto.entelgy.retoentelgy.service.UsuarioService;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class UsuarioServiceImplTest {

	@Autowired
	private UsuarioService usuarioService;

	@Test
	void listarTest() throws Exception {
		GenericoResponse response = usuarioService.listar();

		List<UsuarioDTO> lista = (List<UsuarioDTO>) response.getData();

		Assertions.assertNotNull(response.getData());
		Assertions.assertTrue(lista.size() > 0);

	}
}
